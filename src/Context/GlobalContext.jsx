import { createContext } from "react";
import { useAuth } from "./Hooks/useAuth";
import { useSchoolYears } from "./Hooks/useSchoolYears";
import { useDisciplines } from './Hooks/useDisciplines';
import { useDepartments } from "./Hooks/useDepartments";
import { useTeams } from "./Hooks/useTeams";
import { useStudents } from "./Hooks/useStudents";
import { useCoordinators } from "./Hooks/useCoordinators";
import { useTeachers } from "./Hooks/useTeacher";
import { useCourses } from "./Hooks/useCourses";
import { useProfile } from "./Hooks/useProfille";

const GlobalContext = createContext()

function GlobalProvider({ children }) {
  const { 
    handleLogin, 
    user, 
    token, 
    isLogged, 
    error, 
    setError, 
    errorMessage
  } = useAuth()

  const {
    schoolYears, 
    getSchoolYears
  } = useSchoolYears()

  const {
    disciplines, 
    getDisciplines,
    postDisciplines
  } = useDisciplines()

  const {
    courses,
    getCourses,
    postCourses
  } = useCourses()

  const {
    departments, 
    getDepartments,
    postDepartments
  } = useDepartments()

  const {
    teams, 
    getTeams,
    teamId, 
    getTeamsById
  } = useTeams()

  const {
    students,
    getStudents,
    postStudents
  } = useStudents()

  const {
    coordinatorsCourse, 
    getCoordinatorsCourse, 
    coordinatorsDepartment, 
    getCoordinatorsDepartment,
    postCoordinatorsCourse,
    postCoordinatorsDepartment
  } = useCoordinators()

  const {
    teachers,
    getTeachers,
    postTeachers
  } = useTeachers()

  const {
    profile, 
    editProfile
  } = useProfile()

  return (
    <GlobalContext.Provider 
    value={
      { 
        handleLogin, 
        user, token, 
        isLogged, 
        error, 
        setError, 
        errorMessage,
        schoolYears, 
        getSchoolYears,
        disciplines, 
        getDisciplines,
        departments, 
        getDepartments,
        teams, 
        getTeams,
        teamId, 
        getTeamsById,
        students,
        getStudents,
        coordinatorsCourse, 
        getCoordinatorsCourse, 
        coordinatorsDepartment, 
        getCoordinatorsDepartment,
        teachers,
        getTeachers,
        postDepartments,
        courses,
        getCourses,
        postCourses,
        postDisciplines,
        postCoordinatorsCourse,
        postCoordinatorsDepartment,
        profile, 
        editProfile,
        postStudents,
        postTeachers
      }
      }>
      { children }
    </GlobalContext.Provider>
  )
}

export {GlobalContext, GlobalProvider}