import { useState } from "react";
import { api } from "../../Services/api";

export function useStudents() {
  
  const [students, setStudents] = useState([])
  
  const getStudents = () => {
    
    api.get('/stundents')
    .then(res => {
      setStudents(res.data)
    })
    .catch(err => console.log(err))
  }

  const postStudents = (body) => {
    
    api.post('/stundents', body)
    .then(res => {
      console.log(res.data)
      alert(`Estudante ${res.data.name} criado com sucesso.`)
    })
    .catch(err => console.log(err))
  }

  return { students, getStudents, postStudents}

} 