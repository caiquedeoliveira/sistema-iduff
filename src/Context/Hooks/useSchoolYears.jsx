import { useState } from "react";
import { api } from "../../Services/api";

export function useSchoolYears() {
  
  const [schoolYears, setSchoolYears] = useState([])
  
  const getSchoolYears= () => {
    
    api.get('/schoolyears')
    .then(res => {
      setSchoolYears(res.data)
    })
    .catch(err => console.log(err))
  }

  return { schoolYears, getSchoolYears }

} 