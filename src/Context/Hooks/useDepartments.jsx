import { useState } from "react";
import { api } from "../../Services/api";

export function useDepartments() {
  
  const [departments, setDepartments] = useState([])
  
  const getDepartments = () => {
    
    api.get('/departments')
    .then(res => {
      setDepartments(res.data)
    })
    .catch(err => console.log(err))
  }

  const postDepartments = (body) => {
    
    api.post('/departments', body)
    .then(res => {
      alert(`Departamento ${res.data.name} criado com sucesso.`)
    })
    .catch(err => console.log(err))
  }
  

  return { departments, getDepartments, postDepartments }

} 