import { useState } from "react";
import { api } from "../../Services/api";

export function useProfile() {
  
  const [profile, setProfile] = useState([])
  
  const editProfile = (id, body) => {
    
    api.patch(`/users/${id}`, body)
    .then(res => {
      console.log(res.data)
    })
    .catch(err => console.log(err))
  }

  return { profile, editProfile }

} 