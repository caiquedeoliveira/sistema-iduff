import { useState } from "react";
import { api } from "../../Services/api";

export function useTeams() {
  
  const [teams, setTeams] = useState([])
  const [teamId, setTeamId] = useState([])
  
  const getTeams = () => {
    
    api.get('/teams')
    .then(res => {
      setTeams(res.data)
    })
    .catch(err => console.log(err))
  }

  const getTeamsById = (id) => {
    api.get(`/teams/${id}`)
    .then(res => {
      setTeamId(res.data)
    })
    .catch(err => console.log(err))
  }

  return { teams, getTeams, teamId, getTeamsById }

} 