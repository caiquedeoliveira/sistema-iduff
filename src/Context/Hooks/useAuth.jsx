import { useState } from "react";
import { api } from "../../Services/api";

export function useAuth() {

  const [user, setUser] = useState('')
  const [token, setToken] = useState('')
  const [isLogged, setIsLogged] = useState(false)
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState(false)
 
  const handleLogin = (body, navigate) => {
  
    api.post('/auth/login', body)
    .then(res => {
      setUser(res.data.user)
      setToken(res.data.token)

      // localStorage.setItem('user', JSON.stringify(res.data.user))
      // localStorage.setItem('token', JSON.stringify(res.data.token))

      setIsLogged(true)
      navigate('/curriculos')
      api.defaults.headers.authorization = `bearer ${res.data.token}` 

    })
    .catch(() => {
      setError(true)
      setErrorMessage(true)
    })

  }


  return { handleLogin, user, token, isLogged, error, setError, errorMessage}
}