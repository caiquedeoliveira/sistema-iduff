import { useState } from "react";
import { api } from "../../Services/api";

export function useCourses() {
  
  const [courses, setCourses] = useState([])
  
  const getCourses = () => {
    
    api.get('/courses')
    .then(res => {
      setCourses(res.data)
    })
    .catch(err => console.log(err))
  }

  const postCourses = (body) => {
    
    api.post('/courses', body)
    .then(res => {
      alert(`Curso ${res.data.name} criado com sucesso.`)
    })
    .catch(err => console.log(err))
  }

  return { courses, getCourses, postCourses }

} 