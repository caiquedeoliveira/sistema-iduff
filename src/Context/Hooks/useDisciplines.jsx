import { useState } from "react";
import { api } from "../../Services/api";

export function useDisciplines() {
  
  const [disciplines, setDisciplines] = useState([])
  
  const getDisciplines = () => {
    
    api.get('/disciplines')
    .then(res => {
      setDisciplines(res.data)
    })
    .catch(err => console.log(err))
  }

  const postDisciplines = (body) => {
    
    api.post('/disciplines', body)
    .then(res => {
      console.log(res.data)
      alert(`Disciplina ${res.data.name} criada com sucesso.`)
    })
    .catch(err => console.log(err))
  }


  return { disciplines, getDisciplines, postDisciplines }

} 