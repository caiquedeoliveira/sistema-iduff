import { useState } from "react";
import { api } from "../../Services/api";

export function useCoordinators() {
  
  const [coordinatorsCourse, setCoordinatorsCourse] = useState([])
  
  const getCoordinatorsCourse = () => {
    
    api.get('/coordinators_course')
    .then(res => {
      setCoordinatorsCourse(res.data)
    })
    .catch(err => console.log(err))
  }

  const postCoordinatorsCourse= (body) => {
    
    api.post('/coordinators', body)
    .then(res => {
      console.log(res.data)
      alert(`Coordenador de Curso ${res.data.name} criado com sucesso.`)
    })
    .catch(err => console.log(err))
  }


  const [coordinatorsDepartment, setCoordinatorsDepartment] = useState([])

  const getCoordinatorsDepartment = () => {
    
    api.get('/coordinators')
    .then(res => {
      setCoordinatorsDepartment(res.data)
    })
    .catch(err => console.log(err))
  }

  const postCoordinatorsDepartment = (body) => {
    
    api.post('/coordinators', body)
    .then(res => {
      console.log(res.data)
      alert(`Coordenador de Departamento ${res.data.name} criado com sucesso.`)
    })
    .catch(err => console.log(err))
  }

  return { coordinatorsCourse, getCoordinatorsCourse, coordinatorsDepartment, getCoordinatorsDepartment, postCoordinatorsDepartment, postCoordinatorsCourse }

} 