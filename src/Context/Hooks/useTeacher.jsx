import { useState } from "react";
import { api } from "../../Services/api";

export function useTeachers() {
  
  const [teachers, setTeachers] = useState([])
  
  
  const getTeachers = () => {
    
    api.get('/teachers')
    .then(res => {
      setTeachers(res.data)
    })
    .catch(err => console.log(err))
  }

  const postTeachers = (body) => {
    
    api.get('/teachers', body)
    .then(res => {
      console.log(res.data)
      alert(`Professor ${res.data.name} criado com sucesso.`)
    })
    .catch(err => console.log(err))
  }

  return { teachers, getTeachers, postTeachers }

} 