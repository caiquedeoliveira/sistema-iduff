
import historyIcon from '../Assets/Img/historico.svg'
import classesIcon from '../Assets/Img/turmas.svg'
import resumesIcon from '../Assets/Img/curriculos.svg'
import subscriptionIcon from '../Assets/Img/inscricao.svg'
import calendarIcon from '../Assets/Img/periodo.svg'
import teacherIcon from '../Assets/Img/professores.svg'
import courseIcon from '../Assets/Img/curso.svg'
import departmentIcon from '../Assets/Img/departamento.svg'
import coordinatorsIcon from '../Assets/Img/coordenadores.svg'
import subjectsIcon from '../Assets/Img/materias.svg'

export function NavButtons() {


  const buttons = {
    'history': {
      id: 1,
      name: 'Histórico',
      icon: historyIcon,
      path: '/historico'
    },
    'classes': {
      id: 2,
      name: 'Turmas',
      icon: classesIcon,
      path: '/turmas'
    },
    'resumes': {
      id: 3,
      name: 'Curriculos',
      icon: resumesIcon,
      path: '/curriculos'
    },
    'online_subscription': {
      id: 4,
      name: 'Inscrição Online',
      icon: subscriptionIcon,
      path: '/inscricao-online'
    },
    'year': {
      id: 5,
      name: '2022.1',
      icon: calendarIcon,
      path: '/periodo'
    },
    'teachers': {
      id: 6,
      name: 'Professores',
      icon: teacherIcon,
      path: '/professores'
    },
    'course': {
      id: 7,
      name: 'Curso',
      icon: courseIcon,
      path: '/curso'
    },
    'department': {
      id: 8,
      name: 'Departamento',
      icon: departmentIcon,
      path: '/departamento'
    },
    'coordinators': {
      id: 9,
      name: 'Coordenadores',
      icon: coordinatorsIcon,
      path: '/coordenadores'
    },
    'students': {
      id: 10,
      name: 'Alunos',
      icon: classesIcon,
      path: '/alunos'
    },
    'subjects': {
      id: 11,
      name: 'Matérias',
      icon: subjectsIcon,
      path: '/materias'
    }
  }

  const studentBtn = [buttons.history, buttons.resumes, buttons.classes, buttons.online_subscription]
  const teacherBtn = [buttons.resumes, buttons.classes]
  const coordinatorGmaBtn = [buttons.department, buttons.resumes, buttons.year, buttons.teachers ,buttons.classes, buttons.subjects]
  const coordinatorSiBtn = [buttons.course, buttons.resumes, buttons.students]
  const bossBtn = [buttons.resumes, buttons.department, buttons.course, buttons.coordinators, buttons.year]

  return { studentBtn, teacherBtn, coordinatorGmaBtn, coordinatorSiBtn, bossBtn }
}



  