import axios from "axios";

export const api = axios.create({
  baseURL: 'https://equipe-3-back-end.herokuapp.com',
  // baseURL: 'https://lobinhos.herokuapp.com'
})

