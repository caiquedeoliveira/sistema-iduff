import classes from './Profile.module.css'
import TitleMainContent from '../../Components/TitleMainContent/TitleMainContent'
import Input from '../../Components/Input/Input'
import PrimaryBtn from '../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import { useContext, useEffect, useState } from 'react'
import { GlobalContext } from '../../Context/GlobalContext'

export default function Profile(){

    const context = useContext(GlobalContext)
    const user = context.user

    const [form, setForm] = useState({
        name: user.name,
        street: '',
        street_number: '',
        street_comp: '',
        neighbourhood: '',
        cep: '',
        state: user.state,
        phone: '',
        cellphone: ''
    })

    const body = {
        user: {
            name: form.name,
            street: form.street,
            street_number: form.street_number,
            street_comp: form.street_comp,
            neighbourhood: form.neighbourhood,
            cep: form.cep,
            phone: form.phone,
            cellphone: form.cellphone
        }
    }

    console.log(user)
    return(
        <>
            <main className={classes.main}>
            <TitleMainContent title="Editar perfil" />
            
            <div className={classes.contentBox}>
                <p>Edite os dados cadastrados para sua conta do IDUFF</p>
                <div className={classes.nameBox}>
                    <Input loginInput text="Nome Completo:" placeholder={user.name} onchange={ e => {setForm({...form, name: e.target.value})}}></Input>
                </div>
                <div className={classes.profileInfo}>
                    <p>Nacionalidade: {user.nacionality}</p>
                    <p>Estado: {user.state}</p>
                    <div>
                        <p>RG: {user.rg}</p>
                        <p>CPF: {user.cpf}</p>
                    </div>
                </div>
                
            </div>
            
            <div className={classes.contentBox2}>
            <p>Dados para contato</p>
            <div className={classes.contentInfo2}>
                <Input loginInput text="Rua: " onchange={ e => {setForm({...form, street: e.target.value})}}></Input>
                <Input loginInput text="Nº:" onchange={ e => {setForm({...form, street_number: e.target.value})}}></Input>
                <Input loginInput text="Bairro:" onchange={ e => {setForm({...form, neighbourhood: e.target.value})}}></Input>
            </div>
            <div className={classes.contentInfo3}>
                <Input loginInput text="Complemento" onchange={ e => {setForm({...form, street_comp: e.target.value})}}></Input>
                <Input placeholder={user.state} loginInput text="Estado:" onchange={ e => {setForm({...form, state: e.target.value})}}></Input>
                <Input loginInput text="CEP:" onchange={ e => {setForm({...form, cep: e.target.value})}}></Input>
                <Input loginInput text="Telefone:" onchange={ e => {setForm({...form, phone: e.target.value})}}></Input>
                <Input loginInput text="Celular:" onchange={ e => {setForm({...form, cellphone: e.target.value})}}></Input>
            </div>
            </div>
            <div className={classes.saveBtn}>
                    <PrimaryBtn text="Salvar" btnColor="#008be7" onclick={() => {context.editProfile(user.id, body)}}/>
                </div>
            </main>
        </>
    )
}