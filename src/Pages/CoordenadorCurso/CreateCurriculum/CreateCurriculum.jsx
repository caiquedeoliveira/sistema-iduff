import React, { useState } from 'react'
import classes from './CreateCurriculum.module.css'
import Modal from 'react-modal/lib/components/Modal';
import PrimaryBtn from '../../../Components/Buttons/PrimaryBtn/PrimaryBtn';
import SecondaryBtn from '../../../Components/Buttons/SecondaryBtn/SecondaryBtn';
import ModalCard2 from '../../../Components/ModalCard/ModalCard2';
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent';
import ItemCard from '../../../Components/ListItems/ItemCard/ItemCard'

function CreateCurriculum() {


  const [modalIsOpen, setIsOpen] = useState(false);

  Modal.setAppElement('#root');

  function openModal() {
    setIsOpen(true)
  }

  function closeModal() {
    setIsOpen(false);
  }

  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

  return (
    <div className={classes.main_container}>
        <TitleMainContent title="Cadastro de Currículo" />


       <div className={classes.status_container}>
        <p>Ano letivo atual: 2020.2</p> {/* Vem do back-end */}
        <p>Status: Planejamento</p>
      </div>
      <div  className={classes.subjects_container}>
        <PrimaryBtn text="Adicionar Matéria" btnColor="#008be7" onclick={openModal}/>
      </div>
      <div className={classes.info}>
                <p>Nome</p>
                <p>Área de conhecimento</p>
                <p>Total de vagas</p>
                <p>Período</p>
      </div>
      <div className={classes.listItems}> 
        <ItemCard>
          <p>Cálculo I</p>
          <p>Matemática</p>
          <p>120</p>
          <p>1º período</p>
          <SecondaryBtn text="EDITAR" />
        </ItemCard>
      </div>

      <div className={classes.buttons}>
        <div><PrimaryBtn text="SALVAR" btnColor="#008be7"/></div>
        <div><PrimaryBtn text="CANCELAR" btnColor="#DD0000"/></div>
      </div>

      <Modal
        isOpen={modalIsOpen}
        style={customStyles}
      >
        <ModalCard2 closeModal={closeModal} />
      </Modal>
    </div>
  )
}

export default CreateCurriculum
