import classes from './EditCourse.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import Input from '../../../Components/Input/Input'
import PrimaryBtn from '../../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import { useContext } from 'react'
import { GlobalContext } from '../../../Context/GlobalContext'
import { useState } from 'react'

export  function EditCourse(){

    const context = useContext(GlobalContext)

    const [form, setForm] = useState({
        name: '',
        knowledgeArea: '',
        campus: ''
    })

    const body = {
        "course": { 
                "name": form.name,
                "knowledge_field": form.knowledgeArea,
                "code": "1",
                "campus_address": form.campus
        }
    }

    return(
        <>
            <main className={classes.main}>
            <TitleMainContent title="Curso" />
            
            <div className={classes.contentBox}>
            <p>Edite as informações do curso</p>
                <Input inline text="Nome do Curso:" onchange={ e => {setForm({...form, name: e.target.value})}}></Input>
                <Input inline text="Área de conhecimento:" onchange={ e => {setForm({...form, knowledgeArea: e.target.value})}}></Input>
                <Input inline text="Campus/Sede:" onchange={ e => {setForm({...form, campus: e.target.value})}}></Input>
            </div>
            
            <div className={classes.contentBox2}>
            <p>Dados para contato</p>
            <div className={classes.contentInfo2}>
            <Input inline text="Telefone:"></Input>
            <Input inline text="E-mail:"></Input>
            </div>
            </div>
            <div className={classes.saveBtn}>
                    <PrimaryBtn text="Salvar" btnColor="#008be7" onclick={() => {
                        context.postCourses(body)
                    }}></PrimaryBtn>
                </div>
            </main>
        </>
    )
}