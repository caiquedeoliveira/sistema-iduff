import classes from './CreateAluno.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import Input from '../../../Components/Input/Input'
import PrimaryBtn from '../../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import CloseBtn from '../../../Components/Buttons/CloseBtn/CloseBtn'
import { useNavigate } from 'react-router-dom'
import { useContext, useState } from 'react'
import {GlobalContext} from '../../../Context/GlobalContext'

export function CreateAluno(){

    const context = useContext(GlobalContext)
    const navigate = useNavigate()

    const [form, setForm] = useState({
        name: '',
        nacionality: '',
        state: '', 
        rg: '',
        cpf: '',
        birthday: '',
        registration: '',
        password: '',
    })

    const body = {
        "name": form.name,
        "nacionality": "brasileiro",
        "state": "Rio de Janeiro",
        "rg": form.rg,
        "cpf": form.cpf,
        "email": "teste@lalala.com",
        "birthday": form.birthday,
        "password": form.password,
        "role": 1    
    }

    return(
        <>
            <main className={classes.main}>
            <TitleMainContent title="Alunos" />
            <div className={classes.closeBtn}>
                <CloseBtn onclick={() => {
                    navigate('/alunos')
                }}/>
            </div>
            <div className={classes.contentBox}>
            <p>Curso: Sistemas de Informação</p> 
           <div className={classes.mainInfo}>
                <Input inline text={"Nome:"} onchange={ e => {setForm({...form, name: e.target.value})}}></Input>
                <Input inline text={"RG:"} onchange={ e => {setForm({...form, rg: e.target.value})}}></Input>
           </div>
           <div className={classes.alunoInfo}>
                <Input inline  text={"Nascimento:"} onchange={ e => {setForm({...form, birthday: e.target.value})}}></Input>
                <Input inline text={"Matrícula:"} onchange={ e => {setForm({...form, registration: e.target.value})}}></Input>
                <Input inline text={"Senha:"} onchange={ e => {setForm({...form, password: e.target.value})}}></Input>
                <Input inline text={"CPF:"} onchange={ e => {setForm({...form, cpf: e.target.value})}}></Input>
            </div>
            </div>
            <div className={classes.saveBtn}>
                    <PrimaryBtn text="Salvar" btnColor="#008be7" onclick={() => {
                        context.postStudents(body)
                    }}></PrimaryBtn>
                </div>
            </main>
        </>
    )
}