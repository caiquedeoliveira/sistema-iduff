import classes from './AlunosList.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import BasicCard from '../../../Components/ListItems/BasicCard/BasicCard'
import AddBtn from '../../../Components/Buttons/AddBtn/AddBtn'
import { useNavigate } from "react-router-dom";
import { useContext, useEffect } from 'react';
import {GlobalContext} from '../../../Context/GlobalContext'


export function AlunosList(){

    const navigate = useNavigate()
    const context = useContext(GlobalContext)

    useEffect(() => {
        context.getStudents()
    }, [])

    const students = context.students

    return(
        <>
            <main className={classes.main}>

            <div className={classes.addBtn}>
            <TitleMainContent title="Alunos" />
                <AddBtn onclick={() => {
                    navigate('/alunos/adicionar-aluno')
                }}></AddBtn>
                </div>
            

            <div className={classes.contentInfo}>

            <div className={classes.info}>
                <p>Nome</p>
                <p>CPF</p>
                <p>RG</p>
                <p>Período</p>
            </div>
            <div className={classes.infoCoord}>
                {students.map(student => {
                    return (
                        <BasicCard key={student.id}>
                            <p>{student.name}</p>
                            <p>{student.cpf}</p>
                            <p>{student.rg}</p>
                            <p>3</p>
                        </BasicCard>
                    )
                    
                })

                }
                
            </div>
            </div>
            </main>
        </>
    )
}