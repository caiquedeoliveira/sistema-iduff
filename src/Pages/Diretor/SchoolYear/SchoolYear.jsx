import React, { useContext, useEffect, useState } from 'react'
import classes from './SchoolYear.module.css'
import Modal from 'react-modal/lib/components/Modal';
import SubjectsCard from '../../../Components/ListItems/SubjectsCard/SubjectsCard';
import PrimaryBtn from '../../../Components/Buttons/PrimaryBtn/PrimaryBtn';
import SecondaryBtn from '../../../Components/Buttons/SecondaryBtn/SecondaryBtn';
import ModalCard from '../../../Components/ModalCard/ModalCard';
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent';
import { GlobalContext } from '../../../Context/GlobalContext';

function SchoolYearBoss() {

  const context = useContext(GlobalContext)
  
  const [modalIsOpen, setIsOpen] = useState(false);
  Modal.setAppElement('#root');
  
  useEffect(() => {
    context.getSchoolYears()
  }, [])
  
  const schoolYears = context.schoolYears
  
  function openModal() {
    setIsOpen(true)
  }

  function closeModal() {
    setIsOpen(false);
  }

  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

  return (
    <div className={classes.main_container}>
        <TitleMainContent title="Período Letivo" />


       <div className={classes.status_container}>
        <p>Visualize aqui os períodos letivos da UFF.</p>
      </div>
      <div  className={classes.subjects_container}>
        <PrimaryBtn text="Adicionar Período" btnColor="#008be7" onclick={openModal}/>
      </div>
      <div className={classes.listItems}> 
      {schoolYears.map(schoolYear => {
        return (
          <SubjectsCard>
            <p>2021.1</p>
            <p>Concluído</p>
            <SecondaryBtn text="EDITAR" />
          </SubjectsCard> 
        )
      })}
        
      </div>

      <Modal
        isOpen={modalIsOpen}
        style={customStyles}
      >
        <ModalCard closeModal={closeModal} />
      </Modal>
    </div>
  )
}

export default SchoolYearBoss
