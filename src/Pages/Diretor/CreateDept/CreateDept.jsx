import classes from './CreateDept.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import Input from '../../../Components/Input/Input'
import Select from '../../../Components/Select/Select'
import PrimaryBtn from '../../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import { useContext, useEffect, useState } from 'react'
import { GlobalContext } from '../../../Context/GlobalContext'

export  function CreateDept(){

    const context = useContext(GlobalContext)

    const [form, setForm] = useState({
        name: '',
        knowledgeArea: '',
        campus: ''
    })

    const body = {
        "department": {
            "name": form.name,
            "knowledge_field": form.knowledgeArea,
            "code": "1",
            "campus_address": form.campus
        },
    "coordenator": [
        "must exist"
    ]
} 
    useEffect( () => {
        context.getCoordinatorsDepartment()
    }, [])

    const coords = context.coordinatorsDepartment

    return(
        <>
            <main className={classes.main}>
            <TitleMainContent title="Departamento" />
            
            <div className={classes.contentBox}>
            <p>Edite as informações do departamento</p>
                <Input inline text="Nome do Departamento:" onchange={ e => {setForm({...form, name: e.target.value})}}></Input>
                <Input inline text="Área de conhecimento:" onchange={ e => {setForm({...form, knowledgeArea: e.target.value})}}></Input>
                <Input inline text="Campus/Sede:" onchange={ e => {setForm({...form, campus: e.target.value})}}></Input>
                <Select title="Coordenador" onchange={ e => {setForm({...form, coordinator: e.target.value})}}>
                    {coords.map(coord => {
                        return(
                            <option key={coord.id} value={coord.name}>{coord.name}</option>
                        )
                    })}
                </Select>
            </div>
            
            <div className={classes.contentBox2}>
            <p>Dados para contato</p>
            <div className={classes.contentInfo2}>
            <Input inline text="Telefone:" ></Input>
            <Input inline text="Email:" ></Input>
            </div>
            </div>
            <div className={classes.saveBtn}>
                    <PrimaryBtn text="Salvar" btnColor="#008be7" onclick={() => {
                        context.postDepartments(body)
                    }}></PrimaryBtn>
                </div>
            </main>
        </>
    )
}