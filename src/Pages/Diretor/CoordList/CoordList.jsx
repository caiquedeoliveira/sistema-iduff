import classes from './CoordList.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import BasicCard from '../../../Components/ListItems/BasicCard/BasicCard'
import AddBtn from '../../../Components/Buttons/AddBtn/AddBtn'
import { useContext, useEffect } from 'react'
import {useNavigate} from 'react-router-dom'
import {GlobalContext} from '../../../Context/GlobalContext'

export function CoordList(){

    const context = useContext(GlobalContext)
    const navigate = useNavigate()

    useEffect(() => {
        context.getCoordinatorsCourse()
        context.getCoordinatorsDepartment()
    }, [])

    const coordinatorsCourse = context.coordinatorsCourse
    const coordinatorsDepartment = context.coordinatorsDepartment

    return(
        <>
            <main className={classes.main}>
            <div className={classes.addBtn}>
            <TitleMainContent title="Coordenadores" />
                <AddBtn onclick={() => {
                    navigate('/coordenadores/adicionar-coordenador')
                }}></AddBtn>
            </div>
            

            <div className={classes.contentInfo}>
            
            <div className={classes.info}>
                <p>Nome</p>
                <p>CPF</p>
                <p>RG</p>
                <p>Depart/Curso</p>
            </div>
            <div className={classes.infoCoord}>
                {coordinatorsCourse.map(coordinator => {
                    return (
                        <BasicCard key={coordinator.id}>
                            <p>{coordinator.name}</p>
                            <p>{coordinator.cpf}</p>
                            <p>{coordinator.rg}</p>
                            <p>GMA</p>
                        </BasicCard>
                    )
                    
                })}
                {coordinatorsDepartment.map(coordinator => {
                    return (
                        <BasicCard key={coordinator.id}>
                            <p>{coordinator.name}</p>
                            <p>{coordinator.cpf}</p>
                            <p>{coordinator.rg}</p>
                            <p>GMA</p>
                        </BasicCard>
                    )
                })}
                
            </div>
            </div>
            </main>
        </>
    )
}