import classes from './CreateCourse.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import Input from '../../../Components/Input/Input'
import Select from '../../../Components/Select/Select'
import PrimaryBtn from '../../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import { useContext, useEffect, useState } from 'react'
import { GlobalContext } from '../../../Context/GlobalContext'

export  function CreateCourse(){

    const context = useContext(GlobalContext)

    const [form, setForm] = useState({
        name: '',
        knowledgeArea: '',
        campus: ''
    })

    const body = {
        "course": { 
                "name": form.name,
                "knowledge_field": form.knowledgeArea,
                "code": "1",
                "campus_address": form.campus
        }
    }

    useEffect( () => {
        context.getCoordinatorsCourse()
    }, [])

    const coords = context.coordinatorsCourse


    return(
        <>
            <main className={classes.main}>
            <TitleMainContent title="Curso" />
            
            <div className={classes.contentBox}>
            <p>Edite as informações do curso</p>
                <Input inline text="Nome do Curso:" onchange={ e => {setForm({...form, name: e.target.value})}}></Input>
                <Input inline text="Área de conhecimento:" onchange={ e => {setForm({...form, knowledgeArea: e.target.value})}}></Input>
                <Input inline text="Campus/Sede:" onchange={ e => {setForm({...form, campus: e.target.value})}}></Input>
                <Select title="Coordenador" onchange={ e => {setForm({...form, coordinator: e.target.value})}}>
                {coords.map(coord => {
                        return(
                            <option key={coord.id} value={coord.name}>{coord.name}</option>
                        )
                    })}
                </Select>
            </div>
            
            <div className={classes.contentBox2}>
            <p>Dados para contato</p>
            <div className={classes.contentInfo2}>
            <Input inline text="Telefone:" onchange={ e => {setForm({...form, phone: e.target.value})}}></Input>
            <Input inline text="Email:" onchange={ e => {setForm({...form, email: e.target.value})}}></Input>
            </div>
            </div>
            <div className={classes.saveBtn}>
                    <PrimaryBtn text="Salvar" btnColor="#008be7" onclick={() => {
                        context.postCourses(body)
                    }}>
                    </PrimaryBtn>
                </div>
            </main>
        </>
    )
}