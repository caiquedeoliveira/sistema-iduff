import classes from './CreateCoord.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import Input from '../../../Components/Input/Input'
import Select from '../../../Components/Select/Select'
import PrimaryBtn from '../../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import { useContext, useEffect, useState } from 'react'
import {GlobalContext} from '../../../Context/GlobalContext'
import { ibge } from '../../../Services/ibge'


export function CreateCoord(){

    const context = useContext(GlobalContext)

    const [states, setStates] = useState([])

    const [form, setForm] = useState({
        name: '',
        nacionality: '',
        state: '',
        rg: '',
        cpf: '',
        birthday: '',
        role: '',
        password: ''
    })

    const body = {
        "user" : {
            "name": form.name,
            "nacionality": "brasileiro",
            "state": form.state,
            "rg": form.rg,
            "cpf": form.cpf,
            "birthday": form.birthday,
            "role": form.type,
            "password": form.password
        }
    } 

    const getStates = () => {
        ibge.get('/')
        .then(res => setStates(res.data))
        .catch(err => console.log(err))
    }

    useEffect( () => {
        getStates()
    })


    return(
        <>
            <main className={classes.main}>
            <TitleMainContent title="Coordenadores" />
            <div className={classes.contentBox}>
            
           <div className={classes.mainInfo}>
                <Select title="Tipo:" onchange={ e => {setForm({...form, type: e.target.value})}}>
                    <option value="Departamento">Departamento</option>
                    <option value="Curso">Curso</option>
                </Select>
                <Input grid inline text={"Nome:"} onchange={ e => {setForm({...form, name: e.target.value})}}></Input>
           </div>
           <div className={classes.coordInfo}>
                <Input inline text={"RG:"} onchange={ e => {setForm({...form, rg: e.target.value})}}></Input>
                <Select title="Estado:" >
                {states.map(state => {
                    return(
                        <option key={state.id} value={state.sigla}>{state.sigla}</option>
                    )
                })}
                </Select>
                <Input inline  text={"Nascimento:"} onchange={ e => {setForm({...form, birthday: e.target.value})}}></Input>
                <Input inline text={"Matrícula:"} onchange={ e => {setForm({...form, register: e.target.value})}}></Input>
                <Input inline type='password' text={"Senha:"} onchange={ e => {setForm({...form, password: e.target.value})}}></Input>
                <Input inline text={"CPF:"} onchange={ e => {setForm({...form, cpf: e.target.value})}}></Input>
            </div>
            </div>
            <div className={classes.saveBtn}>
                    <PrimaryBtn text="Salvar" btnColor="#008be7" onclick={() => {
                        if(form.type == "Departamento"){
                            console.log(form.type)
                            context.postCoordinatorsDepartment(body)
                        } else {
                            console.log(form.type)
                            context.postCoordinatorsCourse(body)
                        }
                    }}></PrimaryBtn>
                </div>
            </main>
        </>
    )
}