import React from 'react'
import { useNavigate } from 'react-router-dom'
import PrimaryBtn from '../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import TitleMainContent from '../../Components/TitleMainContent/TitleMainContent'
import classes from './NotFound.module.css'

function NotFound() {

  const navigate = useNavigate()

  function goBack() {
    navigate(-1)
  }

  return (
    <div className={classes.main_container}>
      <TitleMainContent title="Página não encontrada" />
      <PrimaryBtn onclick={goBack} text="voltar" btnColor="#008be7" />
    </div>
  )
}

export default NotFound
