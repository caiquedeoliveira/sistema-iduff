import classes from './TeachersList.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import BasicCard from '../../../Components/ListItems/BasicCard/BasicCard'
import AddBtn from '../../../Components/Buttons/AddBtn/AddBtn'
import { useNavigate } from "react-router-dom";
import { useContext, useEffect } from 'react';
import {GlobalContext} from '../../../Context/GlobalContext';



export function TeachersList(){

    const navigate = useNavigate()

    const context = useContext(GlobalContext)

    useEffect(() => {
        context.getTeachers()
    }, [])

    const teachers = context.teachers

    return(
        <>
            <main className={classes.main}>
            

            <div className={classes.addBtn}>
                <TitleMainContent title="Professores" />
                <AddBtn onclick={() => {
                    navigate('/professores/adicionar-professor')
                }}></AddBtn>
            </div>

            <div className={classes.contentInfo}>

            <div className={classes.info}>
                <p>Nome</p>
                <p>CPF</p>
                <p>RG</p>
                <p>Turmas</p>
            </div>
            <div className={classes.infoProf}>
                {teachers.map(teacher => {
                    return (
                        <BasicCard key={teacher.id}>
                            <p>{teacher.name}</p>
                            <p>{teacher.cpf}</p>
                            <p>{teacher.rg}</p>
                            <p>3</p>
                        </BasicCard>
                    )
                })}
                
            </div>
            </div>
            </main>
        </>
    )
}