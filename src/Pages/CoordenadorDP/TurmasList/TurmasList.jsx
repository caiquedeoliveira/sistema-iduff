import classes from './TurmasList.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import BasicCard from '../../../Components/ListItems/BasicCard/BasicCard'
import AddBtn from '../../../Components/Buttons/AddBtn/AddBtn'
import { useNavigate } from "react-router-dom";
import { useContext, useEffect } from 'react';
import {GlobalContext} from '../../../Context/GlobalContext'

export function TurmasList(){

    const navigate = useNavigate()

    const context = useContext(GlobalContext)

    useEffect(() => {
        context.getTeams()
    }, [])

    const teams = context.teams

    return(
        <>
            <main className={classes.main}>
            
            <div className={classes.addBtn}>
            <TitleMainContent title="Turmas" />
                    <AddBtn onclick={() => {
                        navigate('/turmas/adicionar-turma')
                    }}></AddBtn>
                </div>

            <div className={classes.contentInfo}>
            <p>Ano letivo: 2020.2</p>
            <p>Status: Planejamento</p>

            
            {/* UM ITEM DE TURMA */}
            <div className={classes.turmaItem}>
                <p>Cálculo I</p>
                <div className={classes.info}>
                    <p>Nome</p>
                    <p>Calendário</p>
                    <p>Professor</p>
                    <p>Vagas</p>
                </div>
                <div className={classes.infoProf}>
                    {teams.map(team => {
                        return (
                            <BasicCard key={team.id}>
                                <p>Turma A1</p>
                                <p>{team.calendar}</p>
                                <p>Paulo de Souza</p>
                                <p>3</p>
                            </BasicCard>
                        )
                    })}
                    
                </div>
            </div>
            
            </div>
            </main>
        </>
    )
}