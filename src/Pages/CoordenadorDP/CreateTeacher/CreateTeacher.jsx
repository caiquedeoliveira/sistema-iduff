import classes from './CreateTeacher.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import Input from '../../../Components/Input/Input'
import PrimaryBtn from '../../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import CloseBtn from '../../../Components/Buttons/CloseBtn/CloseBtn'
import Select from '../../../Components/Select/Select'
import SubjectsCard from '../../../Components/ListItems/SubjectsCard/SubjectsCard'
import { useNavigate } from 'react-router-dom'
import { useContext, useState } from 'react'
import { GlobalContext } from '../../../Context/GlobalContext'

export function CreateTeacher(){

    const navigate = useNavigate()
    const context = useContext(GlobalContext)


  const [form, setForm] = useState(
    {
        name: '', 
        rg: '',
        birthday: '',
        registration: '',
        password: '',
        cpf: '',
    }
      )


  const body = {
    teacher: {
        name: form.name,
        rg: form.rg,
        birthday: form.birthday,
        registration: form.registration,
        password: form.password,
        cpf: form.cpf
    }
  } 

    return(
        <>
            <main className={classes.main}>
            <TitleMainContent title="Professores" />
            <div className={classes.closeBtn}>
                <CloseBtn onclick={() => {
                    navigate('/professores')
                }}/>
            </div>
            <div className={classes.contentBox}>
            <p>Departamento: GMA</p> {/** Modificado através dos dados */}
           <div className={classes.mainInfo}>
                <Input grid inline text={"Nome:"} onchange={ e => {setForm({...form, name: e.target.value})}}></Input>
                <Input grid inline text={"RG:"} onchange={ e => {setForm({...form, rg: e.target.value})}}></Input>
           </div>
           <div className={classes.profInfo}>
                <Input inline  text={"Nascimento:"} onchange={ e => {setForm({...form, birthday: e.target.value})}}></Input>
                <Input inline text={"Matrícula:"} onchange={ e => {setForm({...form, registration: e.target.value})}}></Input>
                <Input inline text={"Senha:"} onchange={ e => {setForm({...form, password: e.target.value})}}></Input>
                <Input inline text={"CPF:"} onchange={ e => {setForm({...form, cpf: e.target.value})}}></Input>
            </div>

            <div className={classes.addSubject}>
                <div className={classes.addSubject1}>
                <p>Adicionar matéria para o professor</p>
                    <Select></Select>
                </div>
                <div className={classes.addSubject2}>
                   <PrimaryBtn text="Adicionar" btnColor="#008be7"></PrimaryBtn> 
                </div>
            </div>

            <div className={classes.subjects}>
                <SubjectsCard>
                    <p>Cálculo I</p>
                    <p>Matemática</p>
                    <PrimaryBtn text="EXCLUIR" btnColor="#DD0000"></PrimaryBtn>
                </SubjectsCard>
            </div>


            </div>
            <div className={classes.saveBtn}>
                    <PrimaryBtn text="Salvar" btnColor="#008be7" onclick={() => {
                        context.postTeachers(body) 
                    }}></PrimaryBtn>
                </div>
            </main>
        </>
    )
}