import classes from './CreateTurma.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import Input from '../../../Components/Input/Input'
import Select from '../../../Components/Select/Select'
import PrimaryBtn from '../../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import CloseBtn from '../../../Components/Buttons/CloseBtn/CloseBtn'
import { useNavigate } from 'react-router-dom'
import { useState } from 'react'

export function CreateTurma(){

    const navigate = useNavigate()
    const [form, setForm] = useState({
        name: '',
        rg: '',
        calendar: '',
        subject: '',
        teacher: ''
    })

    const body = {
        turma: {
            name: form.name,
            rg: form.rg,
            calendar: form.calendar,
            subject: form.subject,
            teacher: form.teacher
        }
    }

    return(
        <>
            <main className={classes.main}>
            <TitleMainContent title="Turmas" />
            <div className={classes.closeBtn}>
                <CloseBtn onclick={() => {
                    navigate('/turmas')
                }}/>
            </div>
            <div className={classes.contentBox}>
            <p>Departamento: GMA</p> {/** Modificado através dos dados */}
           <div className={classes.alunoInfo}>
                <Input inline text={"Nome:"} onchange={ e => {setForm({...form, name: e.target.value})}}></Input>
                <Input inline text={"RG:"}> onchange={ e => {setForm({...form, rg: e.target.value})}}</Input>
           </div>
           <div className={classes.mainInfo}>
                <Input inline  text={"Calendário:"} onchange={ e => {setForm({...form, name: e.target.value})}}></Input>
                <Select title={"Matéria"} onchange={ e => {setForm({...form, subject: e.target.value})}}></Select>
                <Select title={"Professor"} onchange={ e => {setForm({...form, teacher: e.target.value})}}></Select>
            </div>
            </div>
            <div className={classes.saveBtn}>
                    <PrimaryBtn text="Salvar" btnColor="#008be7"></PrimaryBtn>
                </div>
            </main>
        </>
    )
}