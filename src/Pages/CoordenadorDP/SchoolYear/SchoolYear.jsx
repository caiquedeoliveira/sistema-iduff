import React from 'react'
import classes from './SchoolYear.module.css'

import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import PrimaryBtn from '../../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import SecondaryBtn from '../../../Components/Buttons/SecondaryBtn/SecondaryBtn'
import BasicCard from '../../../Components/ListItems/BasicCard/BasicCard'
import { useNavigate } from 'react-router-dom'

function SchoolYear() {
  const navigate = useNavigate()

  return (
    <div className={classes.main_container}>
      <TitleMainContent title="Período Letivo" />


      {/* visão do coordenador */}
      <div className={classes.status_container}>
        <p>Ano letivo atual: 2021</p>
        <p>Status: planejamento</p>
      </div>
      <div  className={classes.subjects_container}>
        <p>Matérias oferecidas pelo departamento:</p>
        <PrimaryBtn text="Adicionar Matéria" btnColor="#008be7" onclick={() => {navigate('/materia/adicionar-materia')}} />
      </div>
      <div className={classes.listItems}> 

        {/* planejamento */}
        <BasicCard>
          <p>Cálculo I</p>
          <p>Matemática</p>
          <p>120</p>
          <SecondaryBtn text="EDITAR" />
        </BasicCard>

        {/* inscrição e andamento */}
        <BasicCard>
          <p>Cálculo I</p>
          <p>Matemática</p>
          <p>45</p>
          <p>120</p>
        </BasicCard>

        {/* concluído */}
        <BasicCard>
          <p>Cálculo I</p>
          <p>Matemática</p>
        </BasicCard>
      </div>

    </div>
  )
}

export default SchoolYear
