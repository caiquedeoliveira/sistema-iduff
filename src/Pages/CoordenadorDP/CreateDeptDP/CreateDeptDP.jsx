import classes from './CreateDeptDP.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import Input from '../../../Components/Input/Input'
import Select from '../../../Components/Select/Select'
import PrimaryBtn from '../../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'

export  function CreateDeptDP(){

    const navigate = useNavigate()

    const [form, setForm] = useState({
        name: '',
        knowledgeArea: '',
        campus: '',
        coordinator: '',
        phone: '',
        email: ''
    })

    const body = {
        department: {
            name: form.name,
            knowledgeArea: form.knowledgeArea,
            campus: form.campus,
            coordinator: form.coordinator,
            phone: form.phone,
            email: form.email
        }
    }

    return(
        <>
            <main className={classes.main}>
            <TitleMainContent title="Departamento" />
            
            <div className={classes.contentBox}>
            <p>Edite as informações do departamento</p>
                <Input inline text="Nome do Departamento:" onchange={ e => {setForm({...form, name: e.target.value})}}></Input>
                <Input inline text="Área de conhecimento:" onchange={ e => {setForm({...form, knowledgeArea: e.target.value})}}></Input>
                <Input inline text="Campus/Sede:" onchange={ e => {setForm({...form, campus: e.target.value})}}></Input>
                <Select title="Coordenador" onchange={ e => {setForm({...form, coordinator: e.target.value})}}></Select>
            </div>
            
            <div className={classes.contentBox2}>
            <p>Dados para contato</p>
            <div className={classes.contentInfo2}>
            <Input inline text="Telefone:" onchange={ e => {setForm({...form, phone: e.target.value})}}></Input>
            <Input inline text="Email:" onchange={ e => {setForm({...form, email: e.target.value})}}></Input>
            </div>


            <div className={classes.managementBtns}>
                <PrimaryBtn text="gerenciar matérias" btnColor="#000000" onclick={() => navigate('/materias')}></PrimaryBtn>
                <PrimaryBtn text="visualizar ano letivo" btnColor="#000000" onclick={() => navigate('/periodo')}>visualizar período letivo</PrimaryBtn>
            </div>
            </div>
            <div className={classes.saveBtn}>
                    <PrimaryBtn text="Salvar" btnColor="#008be7"></PrimaryBtn>
                </div>
            </main>
        </>
    )
}