import classes from './CreateSubject.module.css'
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent'
import Input from '../../../Components/Input/Input'
import PrimaryBtn from '../../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import Select from '../../../Components/Select/Select'
import SubjectsCard from '../../../Components/ListItems/SubjectsCard/SubjectsCard'
import { useContext, useState } from 'react'
import { GlobalContext } from '../../../Context/GlobalContext'

export function CreateSubject(){

    const context = useContext(GlobalContext)

    const [form, setForm] = useState({name: '', knowledgeArea: '', hours: ''})

    const body = {
        "discipline": {
            "name": form.name,
            "workload": form.workload,
            "knowledge_field": form.knowledgeArea,
            "subject": [
            "teste"
            ],
            "schoolyear": [
                "1"
            ]
        }
    }
    return(
        <>
            <main className={classes.main}>
            <TitleMainContent title="Adicionar matérias" />
        
            <div className={classes.contentBox}>
           <div className={classes.mainInfo}>
                <Input grid inline text={"Nome:"} onchange={ e => {setForm({...form, name: e.target.value})}}></Input>
           </div>
           <div className={classes.profInfo}>
                <Input inline  text={"Área de conhecimento:"} onchange={ e => {setForm({...form, knowledgeArea: e.target.value})}}></Input>
                <Input inline text={"CHT:"} onchange={ e => {setForm({...form, workload: e.target.value})}}></Input>
            </div>

            <div className={classes.addRequirement}>
                <div className={classes.addRequirement1}>
                <p>Adicionar pré-requisito</p>
                    <Select></Select>
                </div>
                <div className={classes.addRequirement2}>
                   <PrimaryBtn text="Adicionar" btnColor="#008be7"></PrimaryBtn> 
                </div>
            </div>

            <div className={classes.requirement}>
                <SubjectsCard>
                    <p>Cálculo I</p>
                    <p>Matemática</p>
                    <PrimaryBtn text="EXCLUIR" btnColor="#DD0000"></PrimaryBtn>
                </SubjectsCard>
            </div>


            </div>
            <div className={classes.saveBtn}>
                    <PrimaryBtn text="Salvar" btnColor="#008be7" onclick={() => {
                        context.postDisciplines(body)
                    }}></PrimaryBtn>
                </div>
            </main>
        </>
    )
}