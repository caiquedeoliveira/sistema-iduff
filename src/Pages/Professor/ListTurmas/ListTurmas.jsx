import React, { useContext, useEffect } from 'react'
import classes from './ListTurmas.module.css'
import SecondaryBtn from '../../../Components/Buttons/SecondaryBtn/SecondaryBtn';
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent';
import ItemCard from '../../../Components/ListItems/ItemCard/ItemCard'
import { useNavigate } from 'react-router-dom';

import {GlobalContext} from '../../../Context/GlobalContext'

function ListTurmas() {


  const context = useContext(GlobalContext)

  useEffect(() => {
    context.getTeams()
  }, [])

  const teams = context.teams

  const navigate = useNavigate('')
    return (
      <div className={classes.main_container}>
          <TitleMainContent title="Turmas" />


        <div className={classes.status_container}>
          <p>Professor: Fulano da Silva</p> {/* Vem do back-end */}
        </div>
        <div className={classes.info}>
                  <p>Nome</p>
                  <p>Código</p>
                  <p>Calendário</p>
                  <p>Sala</p>
              </div>
        <div className={classes.listItems}> 
        {teams.map(team => {
          return (
            <ItemCard key={team.id}>
              <p>{team.name}</p>
              <p>{team.code}</p>
              <p>{team.calendar}</p>
              <p>{team.room}</p>
              <SecondaryBtn text="GERENCIAR" onclick={ () => {
                  navigate(`/turmas/${team.id}`)
              }} />
            </ItemCard>
          )
        })}
          
        </div>

      </div>
  )
}

export default ListTurmas
