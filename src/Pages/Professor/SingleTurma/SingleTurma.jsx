import React, { useContext, useEffect } from 'react'
import classes from './SingleTurma.module.css'
import SecondaryBtn from '../../../Components/Buttons/SecondaryBtn/SecondaryBtn';
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent';
import ItemCard from '../../../Components/ListItems/ItemCard/ItemCard'
import { useParams } from 'react-router-dom';
import { GlobalContext } from '../../../Context/GlobalContext';

function SingleTurma() {

  const context = useContext(GlobalContext)
  const { id } = useParams()

  useEffect(() => {
    context.getTeamsById(id)
  }, [])
  
  const team = context.teamId

  return (
    <div className={classes.main_container}>
        <TitleMainContent title="Turmas" />


       <div className={classes.status_container}>
        <h1>{team.name}</h1>
        <h3>Turma A1</h3>
        <p>Professor: Fulano da Silva</p>
        <p>Calendário:Seg e Qua, 14h as 16h </p> {/* Vem do back-end */}
      </div>
        <div className={classes.info}>
          <p>Lançamento de notas</p>
        </div>
      <div className={classes.listItems}> 
        <ItemCard>
          <p>José dos Santos</p>
          <p>NOTA P1</p>
          <p>NOTA P2</p>
          <SecondaryBtn text="ENVIAR" />
        </ItemCard>
      </div>

    </div>
  )
}

export default SingleTurma
