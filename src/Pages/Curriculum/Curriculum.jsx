import classes from './Curriculum.module.css'
import TitleMainContent from '../../Components/TitleMainContent/TitleMainContent'
import BasicCard from '../../Components/ListItems/BasicCard/BasicCard'
import Select from '../../Components/Select/Select'
import SubjectsCard from '../../Components/ListItems/SubjectsCard/SubjectsCard'
import { useContext, useEffect } from 'react'
import { GlobalContext } from '../../Context/GlobalContext'

export function Curriculum(){


    const context = useContext(GlobalContext)

    useEffect(() => {
        context.getDisciplines()
    }, [])

    const disciplines = context.disciplines
    const userRole = context.user.role

    return(
        <>
            <main className={classes.main}>
            <TitleMainContent title="Visualizar currículos" />

            <div className={classes.contentInfo}>
            <p>Visualize aqui o currículo de qualquer um dos cursos da UFF.</p>
            <div className={classes.contentSelect}>
            <Select title="Curso">
                {disciplines.map(discipline => {
                    return(
                        <option key={discipline.id} value={discipline.name}>{discipline.name}</option>
                    )
                })}
                
            </Select>
            </div>
            <div className={classes.info}>
                <p>Nome</p>
                <p>Período</p>
                <p>CHT</p>
            </div>
            <div className={classes.infoCurriculo}>
                {disciplines.map(discipline => {
                    return(
                        <BasicCard key={discipline.id}>
                            <p>{discipline.name}</p>

                            {(
                                userRole === 'student' || 
                                userRole === 'teacher' || 
                                userRole === 'coordinatorDP'
                                ) && <p>1º período</p>
                            }

                            {
                                (userRole === 'coordinatorCourse' ||
                                userRole === 'director')
                                && <Select><option value="1">1º período</option><option value="1">2º período</option></Select>
                            }
                            <p>{discipline.workload}</p>
                        </BasicCard>
                    )
                   
                })}
                
            </div>
            </div>
            </main>
        </>
    )
}