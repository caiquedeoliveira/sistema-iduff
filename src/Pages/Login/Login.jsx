import React, { useContext, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import PrimaryBtn from '../../Components/Buttons/PrimaryBtn/PrimaryBtn'
import Input from '../../Components/Input/Input'
import { GlobalContext } from '../../Context/GlobalContext'

import classes from './Login.module.css'

function Login() {

  const context = useContext(GlobalContext)

  const navigate = useNavigate()

  const [loginForm, setLoginForm] = useState({cpf: '', password: ''})

  function handleLoginForm() {
    context.handleLogin(body, navigate)
  }
 
  const body = {
    user: {
      cpf: loginForm.cpf,
      password: loginForm.password
    }
  } 

  const errorMessage = context.errorMessage

  const handleKeypress = e => {
    if (e.key === 'Enter') {
      handleLoginForm() 
    }
  };

  return (
      <div className={classes.background}>
        <section className={classes.login_container}>
          <div className={classes.info_text}>
            <h1>id<b>UFF</b></h1> 
            <h2>Bem vindo ao Sistema Acadêmico de Graduação</h2>
            <p className={classes.login_description}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mattis ex vel mauris porta pulvinar. Integer luctus bibendum ex et faucibus. Vivamus sagittis consectetur quam et rhoncus. Cras imperdiet nisi et elementum dictum. Duis nec turpis eleifend, viverra turpis et, egestas felis.</p>       
          </div>
          <div className={classes.login_form}>
            <h2>Acesse o seu id<b>UFF</b></h2>
            
            <Input loginInput  text="CPF (Somente números)" onkeydown={(e) => {handleKeypress(e)}} onchange={ e => {setLoginForm({...loginForm, cpf: e.target.value})}}/>
            <Input loginInput text="Senha" type="password" onkeydown={(e) => {handleKeypress(e)}} onchange={ e => {setLoginForm({...loginForm, password: e.target.value})}} />
            {errorMessage && (loginForm.cpf === '') && (loginForm.password === '') &&
              <p className={classes.error}>CPF ou senha incorretos</p>
            }
            <div className={classes.buttons}>
              <PrimaryBtn 
                text="Logar" 
                btnColor="#008be7" 
                onclick={(event) => {
                  handleLoginForm()
            
                  context.setError(false)
                }} 
              />
              <p className={classes.forgotPassword}>Esqueci minha senha</p>
            </div>

            
          </div>
        </section>
      </div>

  )
}

export default Login
