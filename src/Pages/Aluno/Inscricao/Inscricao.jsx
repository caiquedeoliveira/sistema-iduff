import React, { useContext, useEffect } from 'react'
import classes from './Inscricao.module.css'
import SecondaryBtn from '../../../Components/Buttons/SecondaryBtn/SecondaryBtn';
import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent';
import ItemCard from '../../../Components/ListItems/ItemCard/ItemCard'

import {GlobalContext} from '../../../Context/GlobalContext'


function Inscricao() {


  const context = useContext(GlobalContext)

  useEffect(() => {
    context.getDisciplines()
  }, [])

  const disciplines = context.disciplines

  return (
    <div className={classes.main_container}>
        <TitleMainContent title="Inscrição em matérias" />
      <div className={classes.info}>
                <p>Nome</p>
                <p>Vagas</p>
                <p>Período</p>
                <p>CHT</p>
      </div>
      <div className={classes.listItems}> 
        {disciplines.map(discipline => {
          return (
            <ItemCard key={discipline.id}>
              <p>{discipline.name}</p>
              <p>13/45</p>
              <p>1º período</p>
              <p>{discipline.workload}</p>
              <SecondaryBtn text="INSCREVER"/>
            </ItemCard>
          )
        })}
        
      </div>

    </div>
  )
}

export default Inscricao
