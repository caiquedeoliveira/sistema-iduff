import React, { useContext, useEffect } from 'react'
import classes from './ListTurmasAluno.module.css'

import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent';
import ItemCard from '../../../Components/ListItems/ItemCard/ItemCard';

import {GlobalContext} from '../../../Context/GlobalContext'

function ListTurmasAluno() {

  const context = useContext(GlobalContext)

  useEffect(() => {
    context.getTeams()
  }, [])

  const teams = context.teams
  // TODO: ver com o back sobre mudar o calendar

  return (
    <div className={classes.main_container}>
        <TitleMainContent title="Turmas" />


       <div className={classes.status_container}>
        <p>Aluno: {context.user.name}</p>
      </div>
      <div className={classes.info}>
                <p>Nome</p>
                <p>Código</p>
                <p>Calendário</p>
                <p>Sala</p>
            </div>
      <div className={classes.listItems}> 
      {teams.map(team => {
        return (
          <ItemCard key={team.id}>
            <p>{team.name}</p>
            <p>{team.code}</p>
            <p>{team.calendar}</p> 
            <p>{team.room}</p>
          </ItemCard>
        )
      })}
        
      </div>

    </div>
  )
}

export default ListTurmasAluno
