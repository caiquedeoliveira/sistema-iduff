import React, { useContext, useEffect } from 'react'
import classes from './Historico.module.css'

import TitleMainContent from '../../../Components/TitleMainContent/TitleMainContent';
import SubjectsCard from '../../../Components/ListItems/SubjectsCard/SubjectsCard';
import { GlobalContext } from '../../../Context/GlobalContext';


function Historico() {

  const context = useContext(GlobalContext)

  useEffect(() => {
    context.getDisciplines()
  }, [])

  const disciplines = context.disciplines

  return (
    <div className={classes.main_container}>
        <TitleMainContent title="Histórico" />

      <div className={classes.info}>
                <p>Nome</p>
                <p>Status</p>
                <p>Nota</p>
            </div>
      <div className={classes.listItems}> 
      {disciplines.map(discipline => {
        return (
          <SubjectsCard key={discipline.id}>
            <p>{discipline.name}</p>
            <p>Aprovado</p>
            <p>9.0</p>
          </SubjectsCard>
        )
      })}
        
      </div>

    </div>
  )
}

export default Historico
