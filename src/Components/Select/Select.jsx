import React from 'react'
import classes from './Select.module.css'

function Select({text, title, children, onchange}) {
  return (
    <>
    <div className={classes.selectContainer}>
      <label>{title}</label>
      <select name="" id="" className={classes.select} onChange={onchange}>
          {children}
        </select>
    </div>
    </>

  )
}

export default Select
