import {HeaderSidebar} from '../HeaderSidebar/HeaderSidebar'
import {NavSidebar} from '../NavSidebar/NavSidebar'
import classes from './Sidebar.module.css'
import uff from '../../Assets/Img/logo-uff.svg'
import { useContext } from 'react'
import { GlobalContext } from '../../Context/GlobalContext'

export function Sidebar(){

    const context = useContext(GlobalContext)
    const user = context.user
    
    return(
        <>
            <aside className={classes.sidebar}>
                <HeaderSidebar name={user.name} role={user.role}/>
                <NavSidebar/>
                <div className={classes.uffLogo}>
                    <img src={uff} alt="Logo da UFF" />
                </div>
            </aside>
        </>
    )
}