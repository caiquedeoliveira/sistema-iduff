import { Outlet } from 'react-router-dom'
import classes from './MainTheme.module.css'
import {Sidebar} from '../Sidebar/Sidebar'

export function MainTheme(){
    return(
        <>
        
        <main className={classes.main}>
        <Sidebar></Sidebar>
            <div className={classes.background}>
                <div className={classes.contentBox}>
                    <Outlet />
                </div>
            </div>
        </main>
        </>
    )
}