import React from 'react'
import classes from './BasicCard.module.css'
function BasicCard({children}) {
  return (
    <div className={classes.card_container}>
      {children}
    </div>
  )
}

export default BasicCard
