import React from 'react'
import Select from '../../Select/Select'

import classes from './SubjectsCard.module.css'

function SubjectsCard({children}) {
  return (
    <div className={classes.card_container}>
      <div className={classes.subject}>
        {children[0]}
      </div>
      <div className={classes.courseTime}>
        {children[1]}
        {children[2]}
      </div>
    </div>
  )
}

export default SubjectsCard
