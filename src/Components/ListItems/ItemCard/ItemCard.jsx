import React from 'react'


import classes from './ItemCard.module.css'

function ItemCard({children}) {
  return (
    <div className={classes.card_container}>

      <div className={classes.courseTime}>
        {children[0]}
        {children[1]}
        {children[2]}
        {children[3]}
        {children[4]}
      </div>
    </div>
  )
}

export default ItemCard
