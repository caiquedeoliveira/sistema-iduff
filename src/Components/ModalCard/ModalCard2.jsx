import React from 'react'
import CloseBtn from '../Buttons/CloseBtn/CloseBtn'
import PrimaryBtn from '../Buttons/PrimaryBtn/PrimaryBtn'
import Input from '../Input/Input'
import Select from '../Select/Select'
import TitleMainContent from '../TitleMainContent/TitleMainContent'
import classes from './ModalCard.module.css'
function ModalCard({closeModal}) {
  return (
    <div className={classes.modalCard}>
      <div className={classes.modal_header}>
        <CloseBtn onclick={closeModal}/>
      
        <TitleMainContent title="Adicionar matéria ao currículo" />
      </div>
      <div className={classes.modal_form}>
        <Select title="Matéria:" />
        <Input text="Período:" inline={true} />  
      </div>
      <div className={classes.button}> 
        <PrimaryBtn text="Confirmar" btnColor="#008be7" />
      </div>
    </div>
  )
}

export default ModalCard
