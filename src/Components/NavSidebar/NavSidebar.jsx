import { NavButton } from "../NavButton/NavButton"
import { NavButtons } from "../../Services/navButtons"
import { useContext, useEffect } from "react"
import { GlobalContext } from "../../Context/GlobalContext"
import { useNavigate } from "react-router-dom"

export function NavSidebar(){

    const navigate = useNavigate()

    const context = useContext(GlobalContext)
    const { studentBtn, teacherBtn, coordinatorGmaBtn, coordinatorSiBtn, bossBtn } = NavButtons()

    const userRole = context.user.role

    return(
        <>
            {userRole === 'student' &&
                studentBtn.map(attributes => {
                return (
                    <NavButton 
                        key={attributes.id}
                        title={attributes.name}
                        image={attributes.icon}
                        onclick={() => {
                            navigate(attributes.path)
                        }}
                    />
                )
                })
            }
            {userRole === 'teacher' &&
                teacherBtn.map(attributes => {
                return (
                    <NavButton 
                        key={attributes.id}
                        title={attributes.name}
                        image={attributes.icon}
                        onclick={() => {
                            navigate(attributes.path)
                        }}
                    />
                )
                })
            }
            {userRole === 'coordinatorDP' &&
                coordinatorGmaBtn.map(attributes => {
                return (
                    <NavButton 
                        key={attributes.id}
                        title={attributes.name}
                        image={attributes.icon}
                        onclick={() => {
                            navigate(attributes.path)
                        }}
                    />
                )
                })
            }
            {userRole === 'coordinatorCourse' &&
                coordinatorSiBtn.map(attributes => {
                return (
                    <NavButton 
                        key={attributes.id}
                        title={attributes.name}
                        image={attributes.icon}
                        onclick={() => {
                            navigate(attributes.path)
                        }}
                    />
                )
                })
            }
            {userRole === 'director' &&
                bossBtn.map(attributes => {
                return (
                    <NavButton 
                        key={attributes.id}
                        title={attributes.name}
                        image={attributes.icon}
                        onclick={() => {
                            navigate(attributes.path)
                        }}
                    />
                )
                })
            }
        </>
    )
}