import classes from './HeaderSidebar.module.css'
import {EditBtn} from '../Buttons/EditBtn/EditBtn'
import { useNavigate } from 'react-router-dom'



export function HeaderSidebar({name, role}){

    let roleScreen = ''

    if (role === 'student') {
        roleScreen = 'Estudante'
    } else if (role === 'teacher') {
        roleScreen = 'Professor(a)'
    } else if (role === 'coordinatorDP') {
        roleScreen = 'Coordenador de Departamento'
    } else if (role === 'coordinatorCourse') {
        roleScreen = 'Coordenador de Curso'
    } else {
        roleScreen = 'Diretor'
    }

    const navigate = useNavigate()

    return(
        <>
            <header className={classes.header}>
                <div className={classes.headerText}>
                    <p>{name}</p>
                    <p>{roleScreen}</p>
                    <p>M. 01301000123</p>
                </div>
                <EditBtn text="editar dados" onclick={() => {
                    navigate('/perfil')
                }}/>

            </header>

        </>
    )
}