import React from 'react'

import classes from "./Input.module.css";

function Input({type, text, inline, onchange, loginInput, grid, onkeydown, value, placeholder}) {

  let flexDirection = 'column'
  let display = 'grid'
  let gridTemplateColumns = '20% 80%'


  if (inline) {
    flexDirection = 'row'
  }

  if (loginInput){
    display = 'flex'
  }

  if (grid){
    gridTemplateColumns = '10% 90%'
  }

  return (
    <div className={classes.input_container} style={{ flexDirection, display, gridTemplateColumns }}>
      <label>{text}</label>
      <input type={type} placeholder={placeholder} onChange={onchange} onKeyDown={onkeydown} value={value}/>
    </div>
  )
}

export default Input
