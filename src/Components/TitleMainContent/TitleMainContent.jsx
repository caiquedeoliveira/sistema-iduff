import React from 'react'

import classes from './TitleMainContent.module.css'

function TitleMainContent({title}) {
  return (
    <div className={classes.header}>
      <h1>{title}</h1>
    </div>
  )
}

export default TitleMainContent
