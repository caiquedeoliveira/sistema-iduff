import classes from './NavButton.module.css'

// Informações virão como props para cada botão
export function NavButton({onclick, image, title}){

    return(
        <>
            <button onClick={onclick} className={classes.navButtons}>
                <img src={image} alt="ícone" />
                <p>{title}</p>
            </button>
        </>
    )
}