import React from 'react'

import classes from './SecondaryBtn.module.css'

function SecondaryBtn({text, onclick}) {
  return (
    <button className={classes.secondaryBtn} onClick={onclick} >
      <p>{text}</p>
    </button>
  )
}

export default SecondaryBtn
