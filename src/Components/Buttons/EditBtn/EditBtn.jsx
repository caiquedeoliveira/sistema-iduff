import React from 'react'

import classes from './EditBtn.module.css'

export function EditBtn({text, onclick}) {
  return (
    <button className={classes.editBtn} onClick={onclick} >
      <p>{text}</p>
    </button>
  )
}

export default EditBtn
