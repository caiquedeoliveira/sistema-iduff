import React from 'react'
import classes from './CloseBtn.module.css'
import closeIcon from '../../../Assets/Img/close-icon.svg'

function CloseBtn({onclick}) {

  return (
    <button className={classes.closeBtn} onClick={onclick}>
      <img src={closeIcon} alt="" />
    </button>
  )
}

export default CloseBtn
