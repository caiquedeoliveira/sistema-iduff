import React, { useState } from 'react';

import classes from './PrimaryBtn.module.css';

function PrimaryBtn({text, btnColor, onclick}) {

  const [bgColor, setBgColor] = useState(btnColor)
  const [txtColor, setTxtColor] = useState('white')

  const style = {
    backgroundColor: bgColor,
    color: txtColor,
    border: `1px solid ${btnColor}`
  }

  return (
    <button 
      style={style} 
      className={classes.primaryBtn}
      onClick={onclick}
      onMouseEnter={() => {
        setBgColor(txtColor);
        setTxtColor(btnColor)
      }}
      onMouseLeave={() => {
        setBgColor(btnColor)
        setTxtColor('white')
      }}
    >
      <p>{text}</p>
    </button>
  );
}

export default PrimaryBtn;