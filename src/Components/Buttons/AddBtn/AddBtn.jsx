import React from 'react'
import classes from './AddBtn.module.css'
import plusIcon from '../../../Assets/Img/plus-icon.svg'

function AddBtn({onclick}) {

  return (
    <button className={classes.addBtn} onClick={onclick}>
      <img src={plusIcon} alt="" />
    </button>
  )
}

export default AddBtn