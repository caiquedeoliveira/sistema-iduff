import PrimaryBtn from "./Components/Buttons/PrimaryBtn/PrimaryBtn";
import SecondaryBtn from "./Components/Buttons/SecondaryBtn/SecondaryBtn";
import EditBtn from "./Components/Buttons/EditBtn/EditBtn";
import Input from "./Components/Input/Input";
import Login from "./Pages/Login/Login";

import { Sidebar } from './Components/Sidebar/Sidebar'

import './App.css'
import SubjectsCard from "./Components/ListItems/SubjectsCard/SubjectsCard";
import CloseBtn from "./Components/Buttons/CloseBtn/CloseBtn";
import AddBtn from "./Components/Buttons/AddBtn/AddBtn";
import { AppRoutes } from "./Routes/Routes";
import { GlobalProvider } from "./Context/GlobalContext";
function App() {
  return (
    <GlobalProvider>
      <AppRoutes />
    </GlobalProvider>
  );
}

export default App;
