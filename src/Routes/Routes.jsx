import { BrowserRouter, Routes, Route, Navigate, Outlet, parsePath } from "react-router-dom";

import Login from '../Pages/Login/Login'
import NotFound from "../Pages/NotFound/NotFound";
import {MainTheme} from '../Components/MainTheme/MainTheme'
import Input from "../Components/Input/Input";
import SchoolYearCordDP from '../Pages/CoordenadorDP/SchoolYear/SchoolYear'
import { useContext, useEffect } from "react";
import { GlobalContext } from "../Context/GlobalContext";
import SchoolYearBoss from "../Pages/Diretor/SchoolYear/SchoolYear";

import { CoordList } from '../Pages/Diretor/CoordList/CoordList'
import {CreateCoord} from '../Pages/Diretor/CreateCoord/CreateCoord'
import {CreateCourse} from '../Pages/Diretor/CreateCourse/CreateCourse'
import {CreateDept} from '../Pages/Diretor/CreateDept/CreateDept'

import {Curriculum} from '../Pages/Curriculum/Curriculum'

import {EditCourse} from '../Pages/CoordenadorCurso/EditCourse/EditCourse'
import { AlunosList } from "../Pages/CoordenadorCurso/AlunosList/AlunosList";
import { CreateAluno } from "../Pages/CoordenadorCurso/CreateAluno/CreateAluno";
import { TeachersList } from "../Pages/CoordenadorDP/TeachersList/TeachersList";
import { CreateTeacher } from "../Pages/CoordenadorDP/CreateTeacher/CreateTeacher";
import CreateCurriculum from "../Pages/CoordenadorCurso/CreateCurriculum/CreateCurriculum";
import { TurmasList } from "../Pages/CoordenadorDP/TurmasList/TurmasList";
import {CreateTurma} from '../Pages/CoordenadorDP/CreateTurma/CreateTurma'
import ListTurmas from '../Pages/Professor/ListTurmas/ListTurmas'
import SingleTurma from "../Pages/Professor/SingleTurma/SingleTurma";
import ListTurmasAluno from "../Pages/Aluno/ListTurmasAluno/ListTurmasAluno";
import Historico from "../Pages/Aluno/Histórico/Historico";
import Inscricao from "../Pages/Aluno/Inscricao/Inscricao";
import Profile from "../Pages/Profile/Profile";
import {CreateSubject} from '../Pages/CoordenadorDP/CreateSubject/CreateSubject';
import { CreateDeptDP } from "../Pages/CoordenadorDP/CreateDeptDP/CreateDeptDP";

export function AppRoutes() {

  const context = useContext(GlobalContext)
  const role = context.user.role
  
  // const isLogged = context.isLogged
  const token = context.token
   
  function LoginRoute() {
  
    return token ? <Navigate to="/"/> : <Outlet/>
  }

  function HomeRoute() {
 
    return token ? <Outlet /> : <Navigate to="/login"/>
  }

  function NotLoggedRoute() {

    return token ? <Outlet /> : <Navigate to="/login"/>
  }


  return (
    <BrowserRouter >
      <Routes>
       
        <Route path="/login" element={<LoginRoute />}>
          <Route path="/login" element={<Login />} />
        </Route>

        <Route path="/" element={<HomeRoute />}>
          <Route path="/" element={<MainTheme />} />
        </Route>
        
          {role === 'student' &&  
          
            <Route path="/" element={<MainTheme />}>
              <Route path="/perfil" element={<Profile />} />
              <Route path="/curriculos" element={<Curriculum />} />
              <Route path="/turmas" element={<ListTurmasAluno />} />
              <Route path="/historico" element={<Historico />} />
              <Route path="/inscricao-online" element={<Inscricao />} />
            </Route>
          
          } 

          {role === 'teacher' &&  
    
            <Route path="/" element={<MainTheme />}>
              <Route path="/perfil" element={<Profile />} />
              <Route path="/curriculos" element={<Curriculum />} />
              <Route path="/turmas" element={<ListTurmas />} />
              <Route path="/turmas/:id" element={<SingleTurma />} />
            </Route>
        
          }

          {role === 'coordinatorDP' &&  
        
            <Route path="/" element={<MainTheme />}>
              <Route path="/perfil" element={<Profile />} />
              <Route path="/curriculos" element={<Curriculum />} />
              <Route path="/departamento" element={<CreateDeptDP />} />
              <Route path="/periodo" element={<SchoolYearCordDP />} />
              <Route path="/professores" element={<TeachersList />} />
              <Route path="/professores/adicionar-professor" element={<CreateTeacher />} />
              <Route path="/turmas" element={<TurmasList />} />
              <Route path="/turmas/adicionar-turma" element={<CreateTurma />} />
              <Route path='/materias' element={<CreateSubject />} />
              <Route path="/materia/adicionar-materia" element={<CreateSubject />} />
            </Route>
    
          }

          {role === 'coordinatorCourse' &&  
        
            <Route path="/" element={<MainTheme />}>
              <Route path="/perfil" element={<Profile />} />
              <Route path="/curriculos" element={<Curriculum />} />
              <Route path="/curso" element={<EditCourse />} />
              <Route path="/alunos" element={<AlunosList />} />
              <Route path="/alunos/adicionar-aluno" element={<CreateAluno />} />
              <Route path="/curriculos" element={<CreateCurriculum />} />
            </Route>
        
          }

          {role === 'director' &&  
              
              <Route path="/" element={<MainTheme />}>
                <Route path="/perfil" element={<Profile />} />
                <Route path="/curriculos" element={<Curriculum />} />
                <Route path="/coordenadores" element={<CoordList />} />
                <Route path="/coordenadores/adicionar-coordenador" element={<CreateCoord />} />
                <Route path="/curso" element={<CreateCourse />} />
                <Route path="/departamento" element={<CreateDept />} />
                <Route path="/periodo" element={<SchoolYearBoss />} />
    
              </Route>
            
          }
    
         
          <Route path="*" element={<NotLoggedRoute />}>
            <Route path="*" element={<MainTheme />}>
              <Route path="*" element={<NotFound />} />
            </Route>
          </Route>
      
      </Routes>
    </BrowserRouter>
  );
}